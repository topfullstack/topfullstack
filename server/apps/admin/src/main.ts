import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  /* 允许跨域 */
  app.enableCors();
  const options = new DocumentBuilder()
    .setTitle('全栈之巅后台管理api')
    .setDescription('供后台管理界面调用的api')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);
  const port = process.env.ADMIN_PORT || 5000
  await app.listen(port, () => {
    console.log(`admin is listing on http://localhost:${port}/api-docs`)
  });
}
bootstrap();
