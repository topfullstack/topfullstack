import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DbModule } from '@libs/db';
import { UsersModule } from './users/users.module';
import { CoursesModule } from './courses/courses.module';
import { EpisodesController } from './episodes/episodes.controller';
import { EpisodesModule } from './episodes/episodes.module';
import { MulterModule } from '@nestjs/platform-express';
import { CommonModule } from '@app/common';

const MAO = require('multer-aliyun-oss')

@Module({
  /* 引入db模块 数据库 */
  imports: [
    CommonModule,
    MulterModule.registerAsync({
      useFactory(){
        return {
          storage: MAO({
            config: {
              region: process.env.OSS_REGION,
              accessKeyId: process.env.OSS_ACCESS_KEY_ID,
              accessKeySecret: process.env.OSS_ACCESS_KEY_SECRET,
              bucket: process.env.OSS_BUCKET
            }
          })
        }
      }
    }),
    // MulterModule.register({
    //   storage: MAO({
    //     config: {
    //       region: 'oss-cn-shenzhen',
    //       accessKeyId: 'LTAI4G5CtM1Pf9JPM1xn7yMU',
    //       accessKeySecret: 'vZs99GEy4FcB0pETSlVxRvvZnw98pS',
    //       bucket: 'topfullstack-hyl'
    //     }
    //   })
    //   // dest:'uploads'
    // }),
    // DbModule,
    UsersModule,
    CoursesModule,
    EpisodesModule
  ],
  controllers: [AppController, EpisodesController],
  providers: [AppService],
})
export class AppModule {}