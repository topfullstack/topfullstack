import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = process.env.ADMIN_PORT || 5001
  await app.listen(port, () => {
    console.log(`app listing on http://localhost:${port}`)
  });
}
bootstrap();
