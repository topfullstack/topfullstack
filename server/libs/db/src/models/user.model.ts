import { prop, modelOptions } from '@typegoose/typegoose'
import { ApiProperty } from '@nestjs/swagger'
/* swagger使用  @modelOptions 模型的其他属性 字段使用添加*/

@modelOptions({
    schemaOptions:{
        timestamps:true
    }
})
export class User{
    @ApiProperty({'description':'用户名',example:'user1'})
    @prop()
    username:string
    
    @ApiProperty({'description':'密码',example:'pass1'})
    @prop()
    password:string
}