import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import './plugins/element'
import './plugins/avue'
import router from './router'

Vue.config.productionTip = false
// import EleForm from 'vue-ele-form'
// // 注册 vue-ele-form
// Vue.use(EleForm)



const http = axios.create({
   baseURL:process.env.VUE_APP_API_URL
})
//avue上传接口使用
Vue.prototype.$httpajax = http;
//平时接口使用
Vue.prototype.$http = http;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
